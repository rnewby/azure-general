﻿
$ErrorActionPreference = "Continue"

Login-AzureRmAccount


(Get-AzureRmSubscription | Out-GridView -Title "Select the Azure subscription that you want to use ..." -PassThru) | Select-AzureRmSubscription


$listusers = "MMalkani1@oxfam.org.uk", `
             "spedio1@oxfam.org.uk", `
             "abatcheloraad@oxfam.org.uk", `
             "rlawson1@oxfam.org.uk", `
             "kmathotage@oxfam.org.uk"


$listRGs = "SingleGivingDevTestNEur-RG1", `
            "SingleGivingProdNEur-RG1", `
            "SharedSqlPaaSDevTestNEur-RG1", `
            "SharedSqlPaaDProdNEur-RG1"

$currroledef = "Contributor"

$currscope
$curruser


foreach ($currrg in $listRGs)
{
    $currscope = "/subscriptions/d5ada694-dadd-4776-b610-a26bd23dba4b/resourceGroups/" + $currrg

    foreach ($curruser in $listusers)
    {
        New-AzureRmRoleAssignment -RoleDefinitionName $currroledef -SignInName $curruser -Scope $currscope

    }


}

