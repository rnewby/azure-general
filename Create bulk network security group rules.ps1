﻿
## Prompt for login credentials always prompt for a log in now
#$azureaccount = Get-AzureRmAccount

#$azureaccount = Get-AzureRmAccount

#if ($azureaccount.Count -eq 0) {Add-AzureRmAccount}


Login-AzureRmAccount;


# Prompt for subscription
$subscriptionId = (Get-AzureRmSubscription | Out-GridView -Title "Select the Azure subscription that you want to use ..." -PassThru).SubscriptionID

# select subscription

#m  Set the the subscription for future actions based on run-time choice
Write-Host "Selecting subscription '$subscriptionId'";
Select-AzureRmSubscription -SubscriptionID $subscriptionId;


#Set some variables ******************************************************************
#$Subscription = "Oxfam GB Azure"

$subnetprefix = "10.145.2.0/24"

#Set a single region
$selectedRegions = "europenorth"

#**************************************************************************************


# Download current list of Azure Public IP ranges
# See this link for latest list

$downloadUri = "https://www.microsoft.com/en-in/download/confirmation.aspx?id=41653"

$downloadPage = Invoke-WebRequest -Uri $downloadUri

$xmlFileUri = ($downloadPage.RawContent.Split('"') -like "https://*PublicIps*")[0]

$response = Invoke-WebRequest -Uri $xmlFileUri


# Get list of regions & public IP ranges

[xml]$xmlResponse = [System.Text.Encoding]::UTF8.GetString($response.Content)


$regions = 
    $xmlResponse.AzurePublicIpAddresses.Region

# Select Azure regions for which to define NSG rules 
#$selectedRegions =
#    $regions.Name |
#    Out-GridView `
#        -Title "Select Azure Datacenter Regions …" `
#        -PassThru




# get the IP ranges into an array
$ipRange = 
    ( $regions | 
      where-object Name -EQ $selectedRegions ).IpRange


#Choose the NSG you are setting - may need a bit of making pretty

$nsgdetails = (Get-AzureRmNetworkSecurityGroup | Out-GridView -Title "Select the Azure network security group that you want to change." -PassThru)

# Set the group to manage using the details selected above.
$nsg = Get-AzureRmNetworkSecurityGroup -ResourceGroupName $nsgdetails.ResourceGroupName -Name $nsgdetails.Name


# Build NSG rules and create them !!!!

# The @ below makes the command run, so be a little careful.


$rules = @()

#enable limiting the number being created, max is 400 for a subscription
$counter = 0

# start priorities at 1500
$rulePriority = 1500

# Loop through each IpRange in the array and do stuff

ForEach ($subnet in $ipRange.Subnet) {

    $ruleName = "Allow_Azure_Out_" + $subnet.Replace("/","-")

    #change counter control here
  if ($counter -lt 400) {


  #remove them to help debug  
#   $rules += Remove-AzureRmNetworkSecurityRuleConfig -NetworkSecurityGroup $nsg `
 #           -Name $ruleName


 # edit existing ones - you get lots of error here if they don't exist, hence the silentcontinue remove to debug?
    If (Get-AzureRmNetworkSecurityRuleConfig -NetworkSecurityGroup $nsg `
        -name $ruleName `
        -ErrorAction SilentlyContinue)
            {"rule exists, change it"
            $rules += Set-AzureRmNetworkSecurityRuleConfig -NetworkSecurityGroup $nsg `
                    -Name $ruleName `
                    -Description "Allow outbound to Azure $subnet" `
                    -Access Allow `
                    -Protocol "*" `
                    -Direction Outbound `
                    -Priority $rulePriority `
                    -SourceAddressPrefix $subnetprefix `
                    -SourcePortRange "*" `
                    -DestinationAddressPrefix "$subnet" `
                    -DestinationPortRange "*"
            }
            else {"Add the rule"
             $rules += Add-AzureRmNetworkSecurityRuleConfig -NetworkSecurityGroup $nsg `
                    -Name $ruleName `
                    -Description "Allow outbound to Azure $subnet" `
                    -Access Allow `
                    -Protocol * `
                    -Direction Outbound `
                    -Priority $rulePriority `
                    -SourceAddressPrefix $subnetprefix `
                    -SourcePortRange * `
                    -DestinationAddressPrefix "$subnet" `
                    -DestinationPortRange *
            }
    
    #print the subnet
    $subnet
      
      # increment the rule priority as they are not allowed to conflict
    $rulePriority++

    # increment the counter
    $counter ++

    }

}

#List the old details
Get-AzureRmNetworkSecurityGroup -ResourceGroupName $nsgdetails.ResourceGroupName -Name $nsgdetails.Name

# Update the network security group rule
Set-AzureRmNetworkSecurityGroup -NetworkSecurityGroup $nsg


#List the new details
Get-AzureRmNetworkSecurityGroup -ResourceGroupName $nsgdetails.ResourceGroupName -Name $nsgdetails.Name

